import React, { Component } from 'react';
import {LineChart, Line, XAxis, YAxis, Tooltip, CartesianGrid } from 'recharts';

var sensors_data;

const data = [
  { name: 'Page A', uv: 1000, pv: 2400, amt: 2400, uvError: [75, 20] },
  { name: 'Page B', uv: 300, pv: 4567, amt: 2400, uvError: [90, 40] },
  { name: 'Page C', uv: 280, pv: 1398, amt: 2400, uvError: 40 },
  { name: 'Page D', uv: 200, pv: 9800, amt: 2400, uvError: 20 },
  { name: 'Page E', uv: 278, pv: null, amt: 2400, uvError: 28 },
  { name: 'Page F', uv: 189, pv: 4800, amt: 2400, uvError: [90, 20] },
  { name: 'Page G', uv: 189, pv: 4800, amt: 2400, uvError: [28, 40] },
  { name: 'Page H', uv: 189, pv: 4800, amt: 2400, uvError: 28 },
  { name: 'Page I', uv: 189, pv: 4800, amt: 2400, uvError: 28 },
  { name: 'Page J', uv: 189, pv: 4800, amt: 2400, uvError: [15, 60] },
];

var data01 = [];

const initialState = {data};


export default class App extends Component {

  render() {
    //retrieve data from external endpoint
    var xhReq = new XMLHttpRequest();
    xhReq.open("GET", 'https://gist.githubusercontent.com/anonymous/a778d7a4ad6a3ca5d8f309aaf4ecb199/raw/3e6064ca5f56435da66f6988243c5e11dd2201ff/blob.json', false);
    xhReq.send(null);
    var jsonObject = JSON.parse(xhReq.responseText);
    sensors_data = jsonObject.data; //data is stored in sensors_data
    console.log(sensors_data);

    data01.push(JSON.stringify(sensors_data));
    document.write(data01[0]);

    const {data} = initialState;
    return (
      <div className='line-charts'>
        <p>LineChart with three y-axes</p>
        <div className='line-chart-wrapper' style={{ margin: 40 }}>
          <LineChart width={600} height={400} data={data}>
            <YAxis type='number' yAxisId={0} domain={[0, 1020]}/>
            <YAxis type='number' orientation='right' yAxisId={1}/>
            <YAxis type='number' orientation='right' yAxisId={2}/>
            <XAxis dataKey='name'/>
            <Tooltip position={{y: 200}} />
            <CartesianGrid stroke='#f5f5f5'/>
            <Line dataKey='uv' stroke='#ff7300' strokeWidth={2} yAxisId={0}/>
            <Line dataKey='pv' stroke='#387908' strokeWidth={2} yAxisId={1}/>
            <Line dataKey='amt' stroke='#38abc8' strokeWidth={2} yAxisId={2}/>
          </LineChart>
        </div>
      </div>
    );
  }
}
